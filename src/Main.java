import java.io.*;
import java.util.ArrayList;
public class Main {
	public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	public static final String engineName = "SuperStrongDinosaurs";
	
	public void printVector(int []v){
		for (int i = 0 ; i< v.length ; i++){
			System.out.print(v[i]+ " ");
		}
	}
	public static void playGame() throws IOException {
		boolean isWhite = false;
		String nextMove, command = "";
		Engine engine = new Engine();
		
		BufferedWriter comenzi = new BufferedWriter(new FileWriter("file.txt"));

		while (true) {

			command = in.readLine();
			comenzi.write(command);
			comenzi.newLine();
			comenzi.flush();

			/*Citim ce ne trimite Winboardul si in functie de ce primim facem
			 * un anumit lucru
			 */
			switch (command) {
			case Commands.EMPTY:
				break;
			case Commands.QUIT:
				/*Iesim*/
				System.exit(0);
				return;
			case Commands.NEW:
				/*Iinitializam motorul nostru de sah*/
				engine = new Engine();
				break;
			case Commands.WHITE:
				/*Punem motorul nostru de sah sa joace cu alb*/
				isWhite = true;
				break;
			case Commands.BLACK:
				/*punem motorul nostru de sah sa joace cu negru*/
				isWhite = false;
				break;
			case Commands.FORCE:
				break;
			case Commands.XBOARD:
				/*Afisam mesajul de mai jos pentru a ne ajuta 
				 * mai tarziu cand primim mutari
				 */
				System.out.println("feature san=0 usermove=0 done=1");
				break;
			case Commands.POST:
				break;
			case Commands.GO:
				/*Facem mutari in functie de culoarea pe care o avem la engine
				 * "go" ne spune ca este randul nostru sa mutam
				 */
				
				/*for (int i = 0 ; i< posibilitati.size() ; i++){
					//System.out.print(posibilitati.get(i).toString()+ " ");
				}*/
				if (isWhite) {
					ArrayList<Search> posibilitati1 = engine.tabladesah.getVector(engine.tabladesah, isWhite);
					engine.vector = posibilitati1 ;
					nextMove = engine.whiteNextMove();
					System.out.println("move " + nextMove);
				}
				ArrayList<Search> posibilitati = engine.tabladesah.getVector(engine.tabladesah, isWhite);
				engine.vector = posibilitati ;
				break;
			case Commands.HARD:
				break;
			default: {
				if(command.matches("\\w\\d\\w\\d")){
					/*Celalalt jucator a facut o miscare 
					 * o procesam si mutam in functie de culoarea pe care o are engine-ul
					 */
					ArrayList<Search> posibilitati1 = engine.tabladesah.getVector(engine.tabladesah, isWhite);
					engine.vector = posibilitati1 ;
					engine.move(command);
					nextMove = engine.getNextMove(isWhite);
					if ( nextMove .equals("resign")){
						System.out.println("resign");
					}
					System.out.println("move " + nextMove);
				}
				else if(command.matches("\\w\\w\\d")){
					ArrayList<Search> posibilitati1 = engine.tabladesah.getVector(engine.tabladesah, isWhite);
					engine.vector = posibilitati1 ;
					engine.move(command);
					nextMove = engine.getNextMove(isWhite);
					if ( nextMove .equals("resign")){
						System.out.println("resign");
					}
					System.out.println("move " + nextMove);
				}
				else if (command.matches("\\w\\w\\d\\w\\d")){
					ArrayList<Search> posibilitati1 = engine.tabladesah.getVector(engine.tabladesah, isWhite);
					engine.vector = posibilitati1 ;
					engine.move(command);
					nextMove = engine.getNextMove(isWhite);
					if ( nextMove .equals("resign")){
						System.out.println("resign");
					}
					System.out.println("move " + nextMove);
				}
				if(command.endsWith("q")){
					/*Celalalt jucator a facut o miscare 
					 * o procesam si mutam in functie de culoarea pe care o are engine-ul
					 */
					ArrayList<Search> posibilitati1 = engine.tabladesah.getVector(engine.tabladesah, isWhite);
					engine.vector = posibilitati1 ;
					engine.move(command);
					nextMove = engine.getNextMove(isWhite);
					if ( nextMove .equals("resign")){
						System.out.println("resign");
					}
					System.out.println("move " + nextMove);
				}
				break;
			}
			}
			
		}
	}

	public static void main(String[] args) throws IOException {
		playGame();
	}
}