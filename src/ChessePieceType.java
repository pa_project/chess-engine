
public enum ChessePieceType {
	PAWN, KNIGHT, BISHOP, ROOK, QUEEN, KING;
}
