
public class Commands {
	public static final String QUIT = "quit";
	public static final String NEW = "new";
	public static final String WHITE = "white";
	public static final String BLACK = "black";
	public static final String GO = "go";
	public static final String RESIGN = "resign";
	public static final String XBOARD = "xboard";
	public static final String POST = "post";
	public static final String HARD = "hard";
	public static final String FORCE = "force";
	public static final String EMPTY = "";

}
