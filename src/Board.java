import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Board{
	public static  Piesa tab[][]= new Piesa [10][10];

	public Board(){
		for (int i = 0 ; i<10;i++){
			tab[0][i]=new Piesa (" ",-1);
			tab[9][i]=new Piesa (" ",-1);
			tab[i][0]=new Piesa (" ",-1);
			tab[i][9]=new Piesa (" ",-1);

		}
		for ( int i  = 1 ; i <= 8; i++){
			for (int j = 1 ; j<= 8 ; j++){
				tab[i][j] = new Piesa("",0);
			}
		}

	}

	public Piesa getPiesa(int n, int m){

		return tab[n][m];

	}
	public  void update( Board tabla,int primacifra,int adouacifra,int atreiacifra,int apatracifra){
		tabla.tab[atreiacifra][apatracifra] = tabla.tab [primacifra][adouacifra];
		tabla.tab[primacifra][adouacifra] = new Piesa("",0);
		tabla.show();


	}
	public void update_white(Board tabla,int primacifra,int adouacifra,int atreiacifra,int apatracifra){
		tabla.tab[9-apatracifra][atreiacifra] = tabla.tab[9-adouacifra][primacifra];
		tabla.tab[9-adouacifra] [primacifra]= new Piesa("",0);
	}

	public ArrayList<Search> getVector( Board tabla,boolean culoare){
		ArrayList<Search> vector = new ArrayList<Search>();

		if(culoare==true){
			for ( int i  = 1; i<= 8;i++){
				for (int j =1 ; j<=8;j++){

					//daca avem culoare alba
					if (tabla.tab[i][j].culoare == 1){
						if ((tabla.tab[i][j].nume  == "Pion" && (tabla.tab[i-1][j].culoare == 0 || tabla.tab[i-1][j-1].culoare == 2 || tabla.tab[i-1][j+1].culoare == 2))
								|| (tabla.tab[i][j].nume  == "Nebun" && (tabla.tab[i-1][j-1].culoare  == 0|| tabla.tab[i-1][j+1].culoare  == 0||tabla.tab[i+1][j+1].culoare  == 0 || tabla.tab[i+1][j-1].culoare  == 0))
								|| (tabla.tab[i][j].nume == "Cal")
								|| (tabla.tab[i][j].nume == "Regina")
								|| (tabla.tab[i][j].nume  == "Tura" && (tabla.tab[i-1][j].culoare  == 0|| tabla.tab[i][j-1].culoare  == 0||tabla.tab[i+1][j].culoare  == 0 || tabla.tab[i][j+1].culoare  == 0))){
							
						Search variabila = new Search();
						variabila.piesa = tabla.tab[i][j];
						variabila.pos = new Pozitie(i,j);
						vector.add(variabila);
					}
				}
			}
			}
		}
		else{
			for ( int i  = 1; i<= 8;i++){
				for (int j =1 ; j<=8;j++){
					if (tabla.tab[i][j].culoare==2){
						if((tabla.tab[i][j].nume  == "Pion" && (tabla.tab[i+1][j].culoare == 0 || tabla.tab[i+1][j+1].culoare == 1 || tabla.tab[i+1][j-1].culoare == 1))
								|| (tabla.tab[i][j].nume  == "Nebun" && (tabla.tab[i+1][j+1].culoare  == 0|| tabla.tab[i+1][j-1].culoare  == 0||tabla.tab[i-1][j-1].culoare  == 0 || tabla.tab[i-1][j+1].culoare  == 0))
								|| (tabla.tab[i][j].nume == "Cal")
								|| (tabla.tab[i][j].nume == "Regina")
								|| (tabla.tab[i][j].nume  == "Tura" && (tabla.tab[i+1][j].culoare  == 0|| tabla.tab[i][j+1].culoare  == 0||tabla.tab[i-1][j].culoare  == 0 || tabla.tab[i][j-1].culoare  == 0))){
							
							Search variabila = new Search();
							variabila.piesa = tabla.tab[i][j];
							variabila.pos = new Pozitie(i,j);
							vector.add(variabila);
						}	
					}
				}

			}
		}
		return vector;
	}


	public void init(){
		for ( int i =1 ; i<9;i++){
			tab[2][i]=new Piesa ("Pion",2); //Culoarea negru este 2
			tab[7][i]=new Piesa ("Pion",1);	//Culoarea alba este cu 1

		}
		tab[1][1]=tab[1][8]=new Piesa ("Tura",2);
		tab[8][1] = tab [8][8]= new Piesa ("Tura",1);
		tab[1][2]=tab[1][7]=new Piesa ("Cal",2);
		tab [8][2]=tab[8][7]=new Piesa ("Cal",1);
		tab[1][3] = tab[1][6]=new Piesa ("Nebun",2);
		tab [8][3]=tab [8][6]=new Piesa ("Nebun",1);
		tab [1][4]=new Piesa ("Regina",2);//Regina de negru
		tab [8][4] =new Piesa ("Regina",1);
		tab [8][5] = new Piesa ("Rege",1);
		tab[1][5]=new Piesa ("Rege",2);

	}
	public void show(){
		for ( int i = 0 ; i< tab.length ; i++){
			for ( int j = 0 ; j< tab.length ; j++){
				System.out.print(tab[i][j]+ " ");
			}
			System.out.println();
		}
	}
	public String toString(){
		String s ="";
		for (int i = 0 ; i<tab.length;i++){
			for ( int j =0 ; j< tab.length;j++){
				s+=(tab[i][j].toString())+" ";
			}
		}
		return s;
	}
}
