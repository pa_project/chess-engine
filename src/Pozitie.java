
public class Pozitie {
	int x;
	int y;
	
	public Pozitie( int x1 , int y1){
		this.x = x1;
		this.y = y1;
	}
	public String toString(){
		String s = "";
		s+=this.x+ " "+this.y+" ";
		return s;
	}
	public void setX(int xnou){
		this.x = xnou;
	}
	public void setY(int ynou){
		this.y = ynou;
	}
	public int getx(){
		return this.x;
	}
	public int gety(){
		return this.y;
	}
}
