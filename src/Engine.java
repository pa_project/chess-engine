/*import java.util.*;


public class Engine {
	public  String starea = new String();
	public  int Culoare,Jucator;
	public  Board tabladesah = new Board();
	public ArrayList<Search> vector = new ArrayList<Search>();
	public  int poz_mutarelin = 2;
	public  int poz_mutarecoloana = 1;
	public  int poz_mutarelin2=7;
	public  int poz_mutarecoloana2 = 1;



	public String getCaracter(int n){
		switch(n){
		case 1:return "a";
		case 2:return "b";
		case 3:return "c";
		case 4:return "d";
		case 5:return "e";
		case 6:return "f";
		case 7:return "g";
		case 8:return "h";
		default:return "0";
		}
	}

	//special number generator Generates a position in a vector 
	//between the first and the last element.
	public int specialRandom(int upper){


		int R = (int) ((Math.random() * (upper-0 ))+0);
		return R;

	}
	In functie de culoarea engine-ului facem o mutare.
	 * Negru  mutam de la a7 in jos
	 * Alb mutam de la a2 in sus
	 
	public String getNextMove(boolean isWhite){
		if(isWhite){
			return whiteNextMove();
		}
		return blackNextMove();
	}

	In blackNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a7 in jos
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 
	public String blackNextMove(){
		String mutare = "";

		int random = specialRandom(vector.size());
		//System.out.println(random);
		//cat timp pionul are in fata obstacol si in lateral nimic
		if (vector.get(random).piesa.nume == "Pion" ){
			while(tabladesah.getPiesa(vector.get(random).pos.x+1, vector.get(random).pos.y).culoare == 1 && tabladesah.getPiesa(vector.get(random).pos.x+1, vector.get(random).pos.y-1).culoare !=2 &&tabladesah.getPiesa(vector.get(random).pos.x+1, vector.get(random).pos.y+1).culoare!=2){
				random = specialRandom(vector.size());
			}

			//System.out.println("esti aici unde ai luat un pion");
			int pozitiex = vector.get(random).pos.x;
			int pozitiey = vector.get(random).pos.y;
			//switch () {
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			if(tabladesah.getPiesa(pozitiex+1,pozitiey-1).culoare == 1){
				mutare += traductorBlack(vector.get(random).pos);


				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);
				mut.pos.setY(pozitiey-1);

				vector.set(random, mut);
				mutare+=traductorBlack(vector.get(random).pos);
				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey-1);
				random = -1;
			}
			else if(tabladesah.getPiesa(pozitiex+1, pozitiey+1).culoare==1){
				mutare += traductorBlack(vector.get(random).pos);

				Search mut = new Search();

				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);
				mut.pos.setY(pozitiey+1);

				vector.set(random,mut);

				mutare+=traductorBlack(vector.get(random).pos);
				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey,pozitiex+1, pozitiey+1);
				random = -1;
			}
			else if(tabladesah.getPiesa(pozitiex+1, pozitiey).culoare == 0){
				mutare = ""+traductorBlack(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);

				vector.set(random, mut);

				//vector.get(random).pos.x ++;

				//System.out.println(vector.get(random).pos.x+"aici ai valoarea lui x");

				mutare += traductorBlack(vector.get(random).pos);

				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey );
				random = -1;

			}
			//are un bug aici .nu uita sa il repari
			else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare == 1){
			//	vector.remove(vector.get(random));
				blackNextMove();
			}
			else if (tabladesah.getPiesa(pozitiex+1, pozitiey).culoare == 1){
				//blackNextMove();
				random++;

			}
			else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare == 0){
				//vector.remove(vector.get(random));
				blackNextMove();
			}
			else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare == 1){
				blackNextMove();
			}

		}

		return mutare;

	}



	In whiteNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a2 in sus
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 
	public String whiteNextMove(){
		String mutare = "";
		int random = specialRandom(vector.size());
		//System.out.println(random);

		if (vector.get(random).piesa.nume == "Pion" ){
			while(tabladesah.getPiesa(vector.get(random).pos.x, vector.get(random).pos.y).culoare == 0 ||tabladesah.getPiesa(vector.get(random).pos.x -1, vector.get(random).pos.y).culoare == 2 && tabladesah.getPiesa(vector.get(random).pos.x-1, vector.get(random).pos.y-1).culoare !=1 && tabladesah.getPiesa(vector.get(random).pos.x-1, vector.get(random).pos.y+1).culoare !=1){
				random = specialRandom(vector.size());
			}
			int pozitiex = vector.get(random).pos.x;
			int pozitiey = vector.get(random).pos.y;
			if(tabladesah.getPiesa(pozitiex-1, pozitiey-1).culoare == 2){
				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);
				mut.pos.setY(pozitiey-1);

				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey-1);
				random = -1;
			}
			else if(tabladesah.getPiesa(pozitiex-1, pozitiey+1).culoare==2){
				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);
				mut.pos.setY(pozitiey+1);
				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey+1);
				random = -1;

			}

			else if (tabladesah.getPiesa(pozitiex-1, pozitiey).culoare == 0){


				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);

				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey);
				random = -1;
			}	
			else if (tabladesah.getPiesa(pozitiex-1, pozitiey).culoare == 2){
				//	vector.remove(vector.get(random));
				random++;
			}
			else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare==2){
				whiteNextMove();
			}
			
			else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare == 0 ){
				//vector.remove(vector.get(random));
				whiteNextMove();
			}
			else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare == 2){
		//		vector.remove(vector.get(random));
				whiteNextMove();
			}

			else{
				whiteNextMove();
			}
			if(tabladesah.getPiesa(pozitiex+1,pozitiey-1).culoare == 1){
				mutare += traductorBlack(vector.get(random).pos);


				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);
				mut.pos.setY(pozitiey-1);

				vector.set(random, mut);
				mutare+=traductorBlack(vector.get(random).pos);
				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey-1);

			}
		}
		return mutare;
	}

	public  void setState(String stare){
		starea=stare;
	}
	Construcotrul nostru pentru aceasta clasa
	 * facem si initierea tablei de sah
	 
	public Engine(){
		Board tabladesah = new Board();

		tabladesah.init();
		Culoare=1; //Punem jucatorul pe negru;
		Jucator=2;
		setState("normal");
	}
	public Engine (int culoareamea){
		Culoare = culoareamea;
		Jucator = 2; //punem jucatorul activ pe 0
		setState("normal");
		Board tabladesah = new Board(); 
		tabladesah.init();
	}
	public static int x =7;

	metoda move ne ajuta sa updatam matricea din spate in functie de ce primim
	 * de la winboard,daca de exemplu primim b2b3 punem piesa 
	 * care era la b2:coloana 2 linia 2 la b3 : coloana 2 linia 3
	 
	public  void move(String mutare){

		//atunci avem o pozitie initiala + pozitie finala
		//avem un string de 4 litere
		//daca de exemplu avem g4f8 separam in g si f si 4 si 8
		int primalitera , adoualitera,opt;
		int pozitie_init , pozitie_fin;

		primalitera = (int)mutare.charAt(0) - 96; //aici mi-ar scoate g-ul
		adoualitera = (int)mutare.charAt(2)-96; //si aici f-ul
		//System.out.println(primalitera + "aceasta este prima litera" + adoualitera + "aceasta este a doua litera");
		opt =mutare.charAt(3);

		pozitie_fin= (int) opt - 48;
		opt = mutare.charAt(1);
		pozitie_init = (int) opt - 48;
		//System.out.println("al doilea parametru este:"+pozitie_init + "al 4 lea este:" + pozitie_fin);
		tabladesah.update_white(tabladesah,primalitera,pozitie_init,adoualitera,pozitie_fin); 



	}

	public String traductorBlack(Pozitie pos ){
		String mutare ="";
		mutare = ""+getCaracter(pos.y) + (9-pos.x);

		return mutare;


	}
	public String traductorWhite(Pozitie pos){
		String mutare ="";
		mutare = "" + getCaracter(pos.y)+ ( 9-pos.x);
		return mutare;
	}
}


		if ( tabladesah.getPiesa(poz_mutarelin2 - 1, poz_mutarecoloana2).culoare == 0 ){
mutare = mutare + getCaracter(poz_mutarecoloana2) + (String) ( "" + (9 - poz_mutarelin2)) + getCaracter(poz_mutarecoloana2) + (String) ( "" + (9 - poz_mutarelin2 +1));
poz_mutarelin2 --;
move(mutare);
return mutare;	
}

else if ( tabladesah.getPiesa(poz_mutarelin2 - 1, poz_mutarecoloana2 + 1).culoare ==2 ){
mutare = mutare + getCaracter(poz_mutarecoloana2) + (String) ( "" + (9 - poz_mutarelin2)) + getCaracter(poz_mutarecoloana2 + 1) + (String) ( "" + (9 - poz_mutarelin2 + 1));
poz_mutarelin2 --;
poz_mutarecoloana2 --;
move(mutare);
return mutare;
}
else if ( tabladesah.getPiesa(poz_mutarelin2 -1, poz_mutarecoloana2 -1).culoare ==2 ){
mutare = mutare + getCaracter(poz_mutarecoloana2) + (String) ( "" + (9 - poz_mutarelin2)) + getCaracter(poz_mutarecoloana2- 1) + (String) ( "" + (9 - poz_mutarelin2 +1));
poz_mutarelin2 --;
poz_mutarecoloana2 ++;
move(mutare);
return mutare;

}
else if (tabladesah.getPiesa(poz_mutarelin+1, poz_mutarecoloana).culoare>0){
return "resign";
}
else if (tabladesah.getPiesa(poz_mutarelin2 - 1, poz_mutarecoloana2).culoare == 2 ){
return "resign";
}
else {
return "resign";
}
 	*/
import java.util.*;


public class Engine {
	public  String starea = new String();
	public  int Culoare,Jucator;
	public  Board tabladesah = new Board();
	public ArrayList<Search> vector = new ArrayList<Search>();
	public  int poz_mutarelin = 2;
	public  int poz_mutarecoloana = 1;
	public  int poz_mutarelin2=7;
	public  int poz_mutarecoloana2 = 1;
	public boolean cul;


	public String getCaracter(int n){
		switch(n){
		case 1:return "a";
		case 2:return "b";
		case 3:return "c";
		case 4:return "d";
		case 5:return "e";
		case 6:return "f";
		case 7:return "g";
		case 8:return "h";
		default:return "0";
		}
	}

	//special number generator Generates a position in a vector 
	//between the first and the last element.
	public int specialRandom(int upper){


		int R = (int) ((Math.random() * (upper-0 ))+0);
		return R;

	}
	/*In functie de culoarea engine-ului facem o mutare.
	 * Negru  mutam de la a7 in jos
	 * Alb mutam de la a2 in sus
	 */
	public String getNextMove(boolean isWhite){
		cul = isWhite;
		if(isWhite){
			return whiteNextMove();
		}
		return blackNextMove();
	}

	/*In blackNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a7 in jos
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 */
	public String blackNextMove(){
		String mutare = "";
		ArrayList<Search> posibilitati1 = tabladesah.getVector(tabladesah, cul);
		vector = posibilitati1 ;
		int random = specialRandom(vector.size());
		//System.out.println(random);
		if (vector.get(random).piesa.nume == "Pion" ){
			//System.out.println("esti aici unde ai luat un pion");
			int pozitiex = vector.get(random).pos.x;
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			if(tabladesah.getPiesa(pozitiex+1, pozitiey).culoare == 0){
				mutare = ""+traductorBlack(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);

				vector.set(random, mut);

				//vector.get(random).pos.x ++;

				//System.out.println(vector.get(random).pos.x+"aici ai valoarea lui x");

				mutare += traductorBlack(vector.get(random).pos);

				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey );
				//random = -1;
				return mutare;
			}
			
			else if(tabladesah.getPiesa(pozitiex+1, pozitiey+1).culoare==1){
				mutare += traductorBlack(vector.get(random).pos);

				Search mut = new Search();

				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);
				mut.pos.setY(pozitiey+1);

				vector.set(random,mut);

				mutare+=traductorBlack(vector.get(random).pos);
				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey,pozitiex+1, pozitiey+1);
				//random = -1;
				return mutare;
			}
			else if(tabladesah.getPiesa(pozitiex+1,pozitiey-1).culoare == 1){
				mutare += traductorBlack(vector.get(random).pos);


				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos ;
				mut.pos.setX(pozitiex+1);
				mut.pos.setY(pozitiey-1);

				vector.set(random, mut);
				mutare+=traductorBlack(vector.get(random).pos);
				//System.out.println("move "+mutare);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex+1,pozitiey-1);
				//random = -1;
				return mutare;
			}
			else {
				return blackNextMove();
			}

		}
		//NEBUNUL DE BOGDAN
		else if(vector.get(random).piesa.nume == "Nebun" && vector.get(random).piesa.culoare == 2){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 1, -1, -1};
			int[] vj = {1, -1, -1, 1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}
		//REGINA DE ALINUTA
		else if(vector.get(random).piesa.nume == "Regina" && vector.get(random).piesa.culoare !=0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 1, -1, -1,1, 0, -1, 0};
			int[] vj = {1, -1, -1, 1,0, 1, 0, -1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}
		//TURA LUI BOGDAN
		else if(vector.get(random).piesa.nume == "Tura" && vector.get(random).piesa.culoare == 2){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0};
			int[] vj = {0, 1, 0, -1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return blackNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else if(vector.get(random).piesa.nume == "Cal" && vector.get(random).piesa.culoare != 0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 2, 2, 1, -1, -2, -2, -1};
			int[] vj = {2, 1, -1, -2, -2, -1, 1, 2};
			
			for(int i = 0; i < 8; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 1)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare==1){
				blackNextMove();
			}
			else{
				blackNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}

		//return mutare;
		else{
			return blackNextMove();
		}
	}
	

	/*In whiteNextMove facem mutarile corespunzatoarea motorului pe culoarea neagra
	 * mutam de la a2 in sus
	 * Ne folosim si de 2 variabile declarate mai sus pentru a face aceste mutari
	 */
	public String whiteNextMove(){
		String mutare = "";
		int random = specialRandom(vector.size());
		if (vector.get(random).piesa.nume == "Pion" ){
			/*while(tabladesah.getPiesa(vector.get(random).pos.x, vector.get(random).pos.y).culoare == 0 ||tabladesah.getPiesa(vector.get(random).pos.x -1, vector.get(random).pos.y).culoare == 2 && tabladesah.getPiesa(vector.get(random).pos.x-1, vector.get(random).pos.y-1).culoare !=1 && tabladesah.getPiesa(vector.get(random).pos.x-1, vector.get(random).pos.y+1).culoare !=1){
				random = specialRandom(vector.size());
			}*/
			int pozitiex = vector.get(random).pos.x;
			int pozitiey = vector.get(random).pos.y;
			if(tabladesah.getPiesa(pozitiex-1, pozitiey-1).culoare == 2){
				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);
				mut.pos.setY(pozitiey-1);

				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey-1);
				//random = -1;
				return mutare;
			}
			else if (tabladesah.getPiesa(pozitiex, pozitiey).culoare == 2){
				return whiteNextMove();
			}
			else if(tabladesah.getPiesa(pozitiex-1, pozitiey+1).culoare==2){
				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);
				mut.pos.setY(pozitiey+1);
				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey+1);
				//random = -1;
				return mutare;
			}

			else if (tabladesah.getPiesa(pozitiex-1, pozitiey).culoare == 0){


				mutare = "" +traductorWhite(vector.get(random).pos);

				Search mut = new Search();
				mut.piesa = vector.get(random).piesa;
				mut.pos = vector.get(random).pos;
				mut.pos.setX(pozitiex-1);

				vector.set(random, mut);
				mutare += traductorWhite(vector.get(random).pos);
				tabladesah.update(tabladesah, pozitiex, pozitiey, pozitiex-1, pozitiey);
				//random = -1;
				return mutare;
			}
			else{return whiteNextMove();}
			
		}
		else if(vector.get(random).piesa.nume == "Nebun" && vector.get(random).piesa.culoare != 0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 1, -1, -1};
			int[] vj = {1, -1, -1, 1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return whiteNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());

			return mutare;
		}	
		else if(vector.get(random).piesa.nume == "Cal" && vector.get(random).piesa.culoare != 0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {-1, -2, -2, -1, 1 , 1 };
			int[] vj = {-2, -1, 1 , 2 , 2 , -2};
			//nu uita ca ai 2 aici
			for(int i = 0; i < 6; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare ==2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}else if(tabladesah.getPiesa(pozitiex, pozitiey).culoare==2){
				whiteNextMove();
			}
			else{
				whiteNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
		
			return mutare;
		}
		else if(vector.get(random).piesa.nume == "Regina" && vector.get(random).piesa.culoare !=0){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0,1, 1, -1, -1};
			int[] vj = {0, 1, 0, -1,1, -1, -1, 1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return whiteNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else if(vector.get(random).piesa.nume == "Tura" && vector.get(random).piesa.culoare == 1){
			int pozitiex = vector.get(random).pos.x;
			int rand = 0;
			ArrayList<Pozitie> mutari = new ArrayList<Pozitie>();
			int pozitiey = vector.get(random).pos.y;
			//System.out.print("move "+traductorBlack(vector.get(random).pos)+getCaracter(pozitiey)+(9- pozitiex -1));
			int[] vi = {1, 0, -1, 0};
			int[] vj = {0, 1, 0, -1};
			
			for(int i = 0; i < 4; i++){
				if(pozitiex + vi[i] > 0 && pozitiex + vi[i] < 9 && pozitiey + vj[i] > 0 && pozitiey + vj[i] < 9)
					if(tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 0 || 
					tabladesah.getPiesa(pozitiex + vi[i], pozitiey + vj[i]).culoare == 2)
						mutari.add(new Pozitie(pozitiex + vi[i], pozitiey + vj[i]));



			}	
			if(!mutari.isEmpty()){		
				rand = specialRandom(mutari.size());
				mutare = ""+traductorBlack(new Pozitie(pozitiex, pozitiey));
				mutare+=traductorBlack(mutari.get(rand));

			}
			else{
				return whiteNextMove();
			}
			
			Search mut = new Search();
			mut.piesa = vector.get(random).piesa;
			mut.pos = vector.get(random).pos ;
			mut.pos.setX(pozitiex + vi[rand]);
			mut.pos.setY(pozitiey + vj[rand]);
			vector.set(random, mut);
			tabladesah.update(tabladesah, pozitiex, pozitiey, mutari.get(rand).getx(), mutari.get(rand).gety());
			return mutare;

		}
		else{return whiteNextMove();}
		
		
	}

	public  void setState(String stare){
		starea=stare;
	}
	/*Construcotrul nostru pentru aceasta clasa
	 * facem si initierea tablei de sah
	 */
	public Engine(){
		Board tabladesah = new Board();

		tabladesah.init();
		Culoare=1; //Punem jucatorul pe negru;
		Jucator=2;
		setState("normal");
	}
	public Engine (int culoareamea){
		Culoare = culoareamea;
		Jucator = 2; //punem jucatorul activ pe 0
		setState("normal");
		Board tabladesah = new Board(); 
		tabladesah.init();
	}
	public static int x =7;

	/*metoda move ne ajuta sa updatam matricea din spate in functie de ce primim
	 * de la winboard,daca de exemplu primim b2b3 punem piesa 
	 * care era la b2:coloana 2 linia 2 la b3 : coloana 2 linia 3
	 */
	public  void move(String mutare){

		//atunci avem o pozitie initiala + pozitie finala
		//avem un string de 4 litere
		//daca de exemplu avem g4f8 separam in g si f si 4 si 8
		int primalitera , adoualitera,opt;
		int pozitie_init , pozitie_fin;

		primalitera = (int)mutare.charAt(0) - 96; //aici mi-ar scoate g-ul
		adoualitera = (int)mutare.charAt(2)-96; //si aici f-ul
		//System.out.println(primalitera + "aceasta este prima litera" + adoualitera + "aceasta este a doua litera");
		opt =mutare.charAt(3);

		pozitie_fin= (int) opt - 48;
		opt = mutare.charAt(1);
		pozitie_init = (int) opt - 48;
		//System.out.println("al doilea parametru este:"+pozitie_init + "al 4 lea este:" + pozitie_fin);
		tabladesah.update_white(tabladesah,primalitera,pozitie_init,adoualitera,pozitie_fin); 



	}

	public String traductorBlack(Pozitie pos ){
		String mutare ="";
		mutare = ""+getCaracter(pos.y) + (9-pos.x);

		return mutare;


	}
	public String traductorWhite(Pozitie pos){
		String mutare ="";
		mutare = "" + getCaracter(pos.y)+ ( 9-pos.x);
		return mutare;
	}
}

